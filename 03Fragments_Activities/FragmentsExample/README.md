# Navigation and Fragments in Android #
## Step-by-step ##

Create a new empty project

**Enable data binding** just like in [02 Data Binding](https://bitbucket.org/freudenthaler_leon/mad_vz_ss20/src/master/02Layouts_DataBinding/)

**Create a new Fragment** by right clicking your project -> New -> Fragment -> Fragment blank (deselect "Include fragment factory mehtods?" checkbox)
> **Note:** Keep "Create layout XML?" checkbox checked, to auto create an XML file for your fragment.

**Enable data binding** in your fragment as well (<layout></layout> tagging in XML!). To integrate binding in your fragment update the code in the onCreateView() method like so:

```kotlin
class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        return binding.root
    }
}
```
This will save an instance of the binding inside a variable as well as return the binding view to the parent activity.
To **Embed the fragment** inside the activity_main.xml place a fragment tag with the name set to the created fragment like this:

```xml
	<androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        tools:context=".MainActivity">

        <fragment
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:name="com.example.fragmentsexample.HomeFragment"></fragment>
    </androidx.constraintlayout.widget.ConstraintLayout>
```

To **enable Android navigation** you must update your gradle files. Inside the project gradle add the version of the navigation component within ext:
```kotlin
//build.gradle(:project)
ext {
        kotlin_version = "1.3.71"
        version_navigation = "1.0.0"
    }
```
Add dependencies of navigation inside the module gradle:
```kotlin
//build.gradle(:app)
dependencies {
    ...other dependencies
    implementation 'androidx.navigation:navigation-fragment-ktx:2.2.2'
    implementation 'androidx.navigation:navigation-ui-ktx:2.2.2'
}
```
> **Note:** The version numbers might differ.

Create a new resource file by right clicking the project -> New -> Android Resource File. Give it a name like "navigation" and set it's resource type to Navigation.
This will create a new folder navigation in the /res directory (if there is none yet) and an XML file. Open the XML file and set the startDestination to your previously created fragment and place the fragment inside the navigation tag (like we did before inside the activity_main.xml):
```xml
	<navigation xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:id="@+id/nav_graph"
    app:startDestination="@id/homeFragment">

        <fragment
            android:id="@+id/homeFragment"
            android:name="com.example.fragmentsexample.HomeFragment"
            android:label="HomeFragment" >
        </fragment>

    </navigation>
```

Now we will have to change our fragment inside activity_main.xml to integrate the new navigation. Change the fragment's name to NavHostFragment and add a navGraph attribute, that references your navigation resource:

```xml
	<androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        tools:context=".MainActivity">

        <fragment
            android:id="@+id/nav_host_fragment_container"
            android:name="androidx.navigation.fragment.NavHostFragment"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            app:defaultNavHost="true"
            app:navGraph="@navigation/navigation"
            >
        </fragment>

    </androidx.constraintlayout.widget.ConstraintLayout>
```
The **NavHostFragment** provides an area within your layout for self-contained navigation to occur. Each NavHostFragment has a [NavController](https://developer.android.com/reference/androidx/navigation/NavController) that defines valid navigation within the navigation host. This includes the [navigation graph](https://developer.android.com/reference/androidx/navigation/NavGraph) as well as navigation state such as current location and back stack that will be saved and restored along with the NavHostFragment itself.

Since we set the startDestination to our fragment, running the app will now show our newly created fragment!

Create another fragment and add it to the navigation.xml. 

	


