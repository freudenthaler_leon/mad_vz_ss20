package com.example.fragmentsexample

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.fragmentsexample.ViewModels.LoginViewModel
import com.example.fragmentsexample.databinding.FragmentHomeBinding

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding
    private val viewModel: LoginViewModel by activityViewModels()       //returns property delegate to access parent activity's ViewModel eg: how to share a view model

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.logoutBtn.setOnClickListener{
           logout()
        }

        viewModel.authenticationState.observe(viewLifecycleOwner, Observer { authenticationState ->
            when (authenticationState) {
                LoginViewModel.AuthenticationState.AUTHENTICATED -> showHomeScreen()        //show welcome text if authenticated
                LoginViewModel.AuthenticationState.UNAUTHENTICATED -> findNavController().navigate(R.id.action_homeFragment_to_loginFragment)
                else -> Log.i("HomeFragment", "New authenticationState doesn't require any UI change")
            }
        })
    }

    private fun logout(){
        //this sets the authenticationState to UNAUTHENTICATED
        //since we observe our authenticationState we will be navigated back to the login screen -> see in observe function above
        viewModel.refuseAuthentication()
    }

    private fun showHomeScreen(){
        //get the username from shared view model and show it to the user
        binding.welcomeText.text = "Welcome " +  viewModel.username + "!"
    }
}
