package com.example.fragmentsexample

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.fragmentsexample.ViewModels.LoginViewModel
import com.example.fragmentsexample.databinding.FragmentLoginBinding
import com.google.android.material.snackbar.Snackbar

/**
 * A simple [Fragment] subclass.
 */
class LoginFragment : Fragment() {
    private lateinit var binding: FragmentLoginBinding                  //data binding
    private val viewModel: LoginViewModel by activityViewModels()       //returns property delegate to access parent activity's ViewModel eg: how to share a view model

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //call viewModel authenticate method when login button is clicked
        binding.loginBtn.setOnClickListener {
            viewModel.authenticate(binding.username.text.toString(), binding.password.text.toString())
        }

        /*
         * since AuthenticationState is of type MutableLiveData we can observe if it has changed
         *
         * this function observes if the authenticationState changed and if so:
         *      go to home fragment if authentication is valid
         *      show an error message if authentication failed
         */
        viewModel.authenticationState.observe(viewLifecycleOwner, Observer { authenticationState ->
            when (authenticationState) {
                LoginViewModel.AuthenticationState.AUTHENTICATED -> findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
                LoginViewModel.AuthenticationState.INVALID_AUTHENTICATION -> showErrorMessage(view)
                else -> Log.e("LoginFragment", "New authenticationState doesn't require any UI change")
            }
        })


        //TODO add button to switch to register fragment
        binding.registerBtn.setOnClickListener{
            findNavController().navigate(R.id.action_loginFragment_to_registrationFragment)
        }
    }

    private fun showErrorMessage(view: View){
        //
        Snackbar.make(view, requireActivity().getString(R.string.login_unsuccessful_msg), Snackbar.LENGTH_LONG).show()
    }
}
