package com.example.fragmentsexample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.get
import com.example.fragmentsexample.ViewModels.LoginViewModel
import com.example.fragmentsexample.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        //get the shared view model
        val viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        Log.i("MainActivity", "Main Activity onCreate() method")
        Log.i("MainActivity", "username: " + viewModel.username)
    }

    override fun onStop() {
        super.onStop()
        Log.i("MainActivity", "Main Activity onStop() method")
    }

    override fun onPause() {
        super.onPause()
        Log.i("MainActivity", "Main Activity onPause() method")
    }
}
