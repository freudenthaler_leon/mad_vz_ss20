# 03 - Fragments and Activities #

This repository provides examples of using fragments:

1. The [NavigationFragmentsExampleBasics](https://bitbucket.org/freudenthaler_leon/mad_vz_ss20/src/master/03Fragments_Activities/NavigationFragmentsExampleBasics/) is a very simple example of navigating between 2 fragments with Android Navigation.

2. The [FragmentsExample](https://bitbucket.org/freudenthaler_leon/mad_vz_ss20/src/master/03Fragments_Activities/FragmentsExample/) project shows a small example of how to implement a login function with Kotlin navigation, SharedViewModel and Observables. It's a single activity app that navigates between fragments based on the authenticationState (authenticated, unauthenticated) provided by the view model, which is shared between fragments and activity. 

3. The third example is a project provided by [Codepath](https://github.com/codepath/android-fragment-basics). It shows how fragments are added dynamically (Fragment Transactions) to arrange them, based on the device screen orientation. 

To learn more about fragments, component architecture and handling different screen sizes, more information can be found on Android Developer site:

[Fragments basics](https://developer.android.com/guide/components/fragments)

[Guide to app architecture](https://developer.android.com/jetpack/docs/guide)

[Handling different screensizes](https://developer.android.com/training/multiscreen/screensizes)

