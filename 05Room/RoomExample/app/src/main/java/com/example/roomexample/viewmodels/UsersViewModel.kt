package com.example.roomexample.viewmodels

import android.app.Application
import android.content.res.Resources
import android.os.Build
import android.text.Html
import android.text.Spanned
import android.util.Log
import androidx.core.text.HtmlCompat
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.example.roomexample.database.AppDatabase
import com.example.roomexample.database.UserDao
import com.example.roomexample.models.User
import kotlinx.coroutines.*
import java.lang.StringBuilder

class UsersViewModel (val database: UserDao, application: Application) : AndroidViewModel(application) {

    private var viewModelJob  = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private val users = database.getAllUsers()

    val userString = Transformations.map(users) { users ->
        formatUsers(users)
    }

    init {
        Log.i("UsersViewModel", "Inside UsersViewModel " + users.toString())
    }

    /*
      this function deletes all users inside the user_table
     */
    fun deleteAllUsers(){
        uiScope.launch {
            withContext(Dispatchers.IO){
                database.clear()
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()   //cancel all coroutines onCleared()
    }

    /*
       This function formats a list of users to HTML
       so it can be rendered easily inside TextView
     */
    private fun formatUsers(users: List<User>) : Spanned {
        val sb = StringBuilder()

        sb.apply {
            append("User List: <br>")
            users.forEach{
                append("Username: ")
                append(it.username + "<br>")
                append("Fullname: ")
                append(it.firstname + " " + it.lastname + "<br><br>")
            }
        }

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            return Html.fromHtml(sb.toString(), Html.FROM_HTML_MODE_LEGACY)
        }else{
            return HtmlCompat.fromHtml(sb.toString(), HtmlCompat.FROM_HTML_MODE_LEGACY)
        }
    }

}