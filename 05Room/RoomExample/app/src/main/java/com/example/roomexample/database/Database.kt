package com.example.roomexample.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.roomexample.models.Exercise
import com.example.roomexample.models.User
import com.example.roomexample.models.UserExercise

@Database(
    entities = [
        Exercise::class,
        User::class,
        UserExercise::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase: RoomDatabase() {
    abstract val userDao: UserDao
    abstract val exerciseDao: ExerciseDao

    companion object{
        // marking the instance as volatile to ensure atomic access to the variable eg.: is always up-to-date (never cached)
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context) : AppDatabase {
            synchronized(this){         //synchronized to prevent multiple threads instantiating at the same time
                var instance = INSTANCE      //use smart casting which is only available to local variables, not class variables

                if(instance == null){        //create a new database if it does not exist
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        "app_database"
                    )
                        .fallbackToDestructiveMigration()   //no migration defined
                        .build()
                    INSTANCE = instance
                }

                return instance
            }
        }






        /*
        private val CALLBACK = object: RoomDatabase.Callback(){
            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)

                val dao = INSTANCE?.userDao
                dao?.clear()
                var user: User = User("user19", "1234", "John", "Doe")
                val result = dao?.insertUser(user)
            }

        }

         */
    }


}