package com.example.roomexample.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders

import com.example.roomexample.R
import com.example.roomexample.database.AppDatabase
import com.example.roomexample.databinding.FragmentUserListBinding
import com.example.roomexample.viewmodels.UsersViewModel
import com.example.roomexample.viewmodels.UsersViewModelFactory

/**
 * A simple [Fragment] subclass.
 */
class UserListFragment : Fragment() {
    private lateinit var binding: FragmentUserListBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_list, container, false)

        //create a reference to the activity this fragment is attached
        val application = requireNotNull(this.activity).application

        //get instance of data dao we want to pass to the viewModelFactory
        val dataSource = AppDatabase.getInstance(application).userDao

        val viewModelFactory = UsersViewModelFactory(dataSource, application)

        //get viewModel instance of viewModelFactory
        val userViewModel = ViewModelProvider(this, viewModelFactory).get(UsersViewModel::class.java)

        binding.usersViewModel = userViewModel  //set binding viewModel to previously created viewModel
        binding.lifecycleOwner = this     //enable binding to observe liveData updates

        binding.clearAllUsersBtn.setOnClickListener {
            userViewModel.deleteAllUsers()
        }

        return binding.root
    }

}
