package com.example.roomexample.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * A basic class representing an entity that is a row in a one-column database table.
 *
 * @ Entity - You must annotate the class as an entity and supply a table name if not class name.
 * @ PrimaryKey - You must identify the primary key.
 * @ ColumnInfo - You must supply the column name if it is different from the variable name.
 *
 */


@Entity(tableName = "user_table")   //tableName is optional
data class User (
    @ColumnInfo(name = "username")
    var username: String,

    @ColumnInfo(name = "password")
    var password: String,

    @ColumnInfo(name = "firstname")
    var firstname: String = "",

    @ColumnInfo(name = "lastname")
    var lastname: String = ""
){
    @PrimaryKey(autoGenerate = true)
    var userId: Long = 0L
}