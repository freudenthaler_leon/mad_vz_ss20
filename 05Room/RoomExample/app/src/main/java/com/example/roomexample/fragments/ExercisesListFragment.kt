package com.example.roomexample.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil

import com.example.roomexample.R
import com.example.roomexample.databinding.FragmentExercisesListBinding

/**
 * A simple [Fragment] subclass.
 */
class ExercisesListFragment : Fragment() {
    private lateinit var binding: FragmentExercisesListBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_exercises_list, container, false)
        return binding.root
    }

}
