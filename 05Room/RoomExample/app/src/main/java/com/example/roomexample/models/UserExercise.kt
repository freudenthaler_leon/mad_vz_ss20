package com.example.roomexample.models

import androidx.room.Entity

/*
 *  Associative table for m:n relation between users and exercises
 *  Multiple users can do multiple exercises
 */
@Entity(tableName = "user_exercise_table", primaryKeys = ["userId", "exerciseId"])
data class UserExercise(
    val userId: Long,
    val exerciseId: Long,
    var completed: Boolean = false
)