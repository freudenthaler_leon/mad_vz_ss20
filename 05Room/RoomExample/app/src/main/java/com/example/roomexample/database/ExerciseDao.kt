package com.example.roomexample.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.roomexample.models.Exercise

@Dao
interface ExerciseDao {
    @Insert
    fun insertExercise(exercise: Exercise)

    @Query("SELECT * FROM exercise_table")
    fun getAllExercises() : LiveData<List<Exercise>>

    @Query("Select * FROM exercise_table WHERE exerciseId = :exerciseId")
    fun getExercise(exerciseId: Long) : Exercise
}