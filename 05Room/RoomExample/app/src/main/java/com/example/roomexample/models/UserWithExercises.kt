package com.example.roomexample.models

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

data class UserWithExercises (
    @Embedded val user: User,

    @Relation(
        parentColumn = "userId",
        entityColumn = "exerciseId",
        associateBy = Junction(UserExercise::class)
    )
    val exercises: List<Exercise>
)