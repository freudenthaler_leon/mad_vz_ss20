package com.example.roomexample.viewmodels

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import com.example.roomexample.database.UserDao
import com.example.roomexample.models.User
import kotlinx.coroutines.*

class RegisterViewModel(val database: UserDao, application: Application) : AndroidViewModel(application) {

    private var viewModelJob  = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    fun createUser(user: User) {
        uiScope.launch {
            val user = user
            withContext(Dispatchers.IO){
                val result = database.insertUser(user)
                Log.i("RegisterViewModel", result.toString())
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()   //cancel all coroutines onCleared()
    }
}
