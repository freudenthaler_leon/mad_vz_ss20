package com.example.roomexample.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.roomexample.models.User
import com.example.roomexample.models.UserWithExercises

@Dao
interface UserDao {

    @Insert
    fun insertUser(user: User):Long

    @Update
    fun updateUser(user: User)

    @Query("SELECT * from user_table WHERE userId = :userId")
    fun getUser(userId: Long): User

    @Delete
    fun deleteUser(user: User)

    @Query("DELETE FROM user_table")
    fun clear()

    // LiveData is a data holder class that can be observed within a given lifecycle.
    // Always holds/caches latest version of data. Notifies its active observers when the
    // data has changed. Since we are getting all the contents of the database,
    // we are notified whenever any of the database contents have changed.
    @Query("SELECT * FROM user_table ORDER BY userId ASC")
    fun getAllUsers(): LiveData<List<User>>

    @Transaction
    @Query("Select * FROM user_table WHERE userId = :userId")
    fun getUserExercises(userId: Long): LiveData<List<UserWithExercises>>

}