package com.example.roomexample.fragments

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController

import com.example.roomexample.R
import com.example.roomexample.database.AppDatabase
import com.example.roomexample.databinding.RegisterFragmentBinding
import com.example.roomexample.models.User
import com.example.roomexample.viewmodels.RegisterViewModel
import com.example.roomexample.viewmodels.RegisterViewModelFactory

class RegisterFragment : Fragment() {
    private lateinit var binding: RegisterFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.register_fragment, container, false)

        //create a reference to the activity this fragment is attached
        val application = requireNotNull(this.activity).application

        //get instance of data dao we want to pass to the viewModelFactory
        val dataSource = AppDatabase.getInstance(application).userDao

        //used to enable data passing to the viewModel
        val viewModelFactory = RegisterViewModelFactory(dataSource, application)

        //get viewModel instance of viewModelFactory
        val registerViewModel = ViewModelProvider(this, viewModelFactory).get(RegisterViewModel::class.java)

        binding.lifecycleOwner = this     //enable binding to observe liveData updates

        binding.registerBtn.setOnClickListener {
            registerViewModel.createUser(getUserData())
            findNavController().navigate(R.id.action_registerFragment_to_userListFragment)
        }

        return binding.root
    }

    private fun getUserData() : User {
        //TODO check if fields are empty
        var user: User = User(
            binding.usernameText.text.toString(),
            binding.passwordText.text.toString(),
            binding.firstnameText.text.toString(),
            binding.lastnameText.text.toString()
        )
        return user
    }

}
