package com.example.libraryroomexample.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager

import com.example.libraryroomexample.R
import com.example.libraryroomexample.adapters.WordListAdapter
import com.example.libraryroomexample.databinding.FragmentWordListBinding
import com.example.libraryroomexample.viewmodels.WordViewModel

/**
 * A simple [Fragment] subclass.
 */
class WordListFragment : Fragment() {
    private lateinit var binding: FragmentWordListBinding
    private val viewModel: WordViewModel by activityViewModels()        //shared view model between fragments

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_word_list, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val recyclerView = binding.recyclerview
        val adapter = WordListAdapter()    //pass parent activity context to adapter
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireActivity())

        // on click listener for navigation to AddWordFragment
        binding.addWordBtn.setOnClickListener{
            findNavController().navigate(R.id.action_wordListFragment_to_addWordFragment)
        }

        //observer to set words in adapter if the words from database change
        viewModel.allWords.observe(viewLifecycleOwner, Observer { words ->
            adapter.words = words
        })
    }
}
