package com.example.libraryroomexample.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController

import com.example.libraryroomexample.R
import com.example.libraryroomexample.databinding.FragmentAddWordBinding
import com.example.libraryroomexample.models.Word
import com.example.libraryroomexample.viewmodels.WordViewModel

/**
 * A simple [Fragment] subclass.
 */
class AddWordFragment : Fragment() {
    private lateinit var binding: FragmentAddWordBinding
    private val viewModel: WordViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_word, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /**
         * Save word to db on button click
         */
        binding.buttonSave.setOnClickListener {
            if(binding.editWord.text.toString().isNotEmpty()){
                var word = Word(binding.editWord.text.toString())
                viewModel.insert(word)
                findNavController().navigate(R.id.action_addWordFragment_to_wordListFragment)   //navigate back to list
            }else{
                Toast.makeText(context, "Word cannot be empty", Toast.LENGTH_LONG).show()
            }
        }
    }
}
