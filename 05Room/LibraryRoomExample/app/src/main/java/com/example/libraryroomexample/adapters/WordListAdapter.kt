package com.example.libraryroomexample.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.libraryroomexample.R
import com.example.libraryroomexample.models.Word

class WordListAdapter : RecyclerView.Adapter<WordListAdapter.WordViewHolder>() {

     var words = emptyList<Word>() // Cached copy of words
            set(value) {
                field = value
                notifyDataSetChanged()  // Notify any registered observers that the data set has changed.
            }

    /**
     * A nested class marked as inner can access the members of its outer class. Inner classes carry a reference to an object of an outer class:
     */
    inner class WordViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val wordItemView: TextView = itemView.findViewById(R.id.textView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WordViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = layoutInflater.inflate(R.layout.word_item, parent, false)
        return WordViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: WordViewHolder, position: Int) {
        val current = words[position]
        holder.wordItemView.text = current.word
    }

    override fun getItemCount() = words.size
}