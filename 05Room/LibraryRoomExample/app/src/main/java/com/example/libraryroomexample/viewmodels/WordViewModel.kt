package com.example.libraryroomexample.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.libraryroomexample.database.WordDatabase
import com.example.libraryroomexample.models.Word
import com.example.libraryroomexample.repositories.WordRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


/*
    The ViewModel's role is to provide data to the UI and survive configuration changes.
    A ViewModel acts as a communication center between the Repository and the UI.
    You can also use a ViewModel to share data between fragments.
 */
class WordViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: WordRepository
    // Using LiveData and caching what getAlphabetizedWords returns has several benefits:
    // - We can put an observer on the data (instead of polling for changes) and only update the
    //   the UI when the data actually changes.
    // - Repository is completely separated from the UI through the ViewModel.
    val allWords: LiveData<List<Word>>

    init {
        val wordsDao = WordDatabase.getDatabase(application, viewModelScope).wordDao()
        repository = WordRepository(wordsDao)
        allWords = repository.allWords
    }

    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     *
     * In Kotlin, all coroutines run inside a CoroutineScope.
     * A scope controls the lifetime of coroutines through its job.
     * When you cancel the job of a scope, it cancels all coroutines started in that scope.
     *
     * viewModelScope as an extension function of the ViewModel class, enabling you to work with scopes.
     */
    fun insert(word: Word) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(word)
    }

    /* same as insert() function
    fun insertWord(word: Word){
        viewModelScope.launch {
            repository.insert(word)
        }
    }
     */
}