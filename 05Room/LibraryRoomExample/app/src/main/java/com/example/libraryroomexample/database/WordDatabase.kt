package com.example.libraryroomexample.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.libraryroomexample.models.Word
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

// Annotates class to be a Room Database with a table (entity) of the Word class
@Database(
    entities = arrayOf(Word::class),    // Add additional entities here
    version = 1,                        // When you modify the database schema, you'll need to update the version number and define a migration strategy
    exportSchema = false)
public abstract class WordDatabase : RoomDatabase() {
    // add all daos as abstract functions
    abstract fun wordDao(): WordDao

    // Singleton prevents multiple instances of database opening at the
    // same time.
    companion object {
        // marking the instance as volatile to ensure atomic access to the variable eg.: is always up-to-date (never cached)
        @Volatile
        private var INSTANCE: WordDatabase? = null

        fun getDatabase(context: Context, scope: CoroutineScope): WordDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {                    //synchronized to prevent multiple threads instantiating at the same time
                val instance = Room.databaseBuilder(     //create a new database class if it does not exist
                    context.applicationContext,
                    WordDatabase::class.java,
                    "word_database"
                )
                    .addCallback(WordDatabaseCallback(scope))   //callback
                    .build()
                INSTANCE = instance
                return instance
            }
        }
    }

    /**
     * This callback function is called when DB is opened (on each app start)
     * It is used to delete all entries from word_table and create 2 demo entries
     */
    private class WordDatabaseCallback(
        private val scope: CoroutineScope
    ) : RoomDatabase.Callback() {
        override fun onOpen(db: SupportSQLiteDatabase) {
            super.onOpen(db)
            INSTANCE?.let { database ->         //the INSTANCE?.let is used to check if INSTANCE is null (if not null populate database)
                scope.launch {
                    populateDatabase(database.wordDao())
                }
            }
        }

        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            //if demo entries should only be created at first DB launch add the populateDatabase() function here
        }

        suspend fun populateDatabase(wordDao: WordDao) {
            // Delete all content on database create
            wordDao.deleteAll()

            // Add sample words
            var word = Word("Hello")
            wordDao.insertWord(word)
            word = Word("World!")
            wordDao.insertWord(word)
        }
    }

}



