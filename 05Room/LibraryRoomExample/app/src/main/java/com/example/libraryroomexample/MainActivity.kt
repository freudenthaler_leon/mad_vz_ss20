package com.example.libraryroomexample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.libraryroomexample.viewmodels.WordViewModel

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
