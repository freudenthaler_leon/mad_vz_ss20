package com.example.libraryroomexample.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.libraryroomexample.models.Word

/*
In the DAO (data access object), you specify SQL queries and associate them with method calls.
The compiler checks the SQL and generates queries from convenience annotations for common queries, such as @Insert.
Room uses the DAO to create a clean API for your code.
 */
@Dao
interface WordDao {

    // LiveData is a data holder class that can be observed within a given lifecycle.
    // Always holds/caches latest version of data. Notifies its active observers when the
    // data has changed. Since we are getting all the contents of the database,
    // we are notified whenever any of the database contents have changed.
    @Query("SELECT * from word_table ORDER BY word ASC")
    fun getAlphabetizedWords(): LiveData<List<Word>>

    // Insert a new word
    // A suspending function is simply a function that can be paused and resumed at a later time.
    // They can execute a long running operation and wait for it to complete without blocking.
    // Suspending functions can only be invoked by another suspending function or within a coroutine.
    @Insert
    suspend fun insertWord(word: Word)

    @Update
    fun updateWord(word: Word)

    @Delete
    fun deleteWord(word: Word)

    // Delete all words in table
    // There is no convenience annotation for deleting multiple entities, so it's annotated with the generic @Query
    @Query("DELETE FROM word_table")
    suspend fun deleteAll()

    // Get a word by id
    @Query("SELECT * from word_table WHERE wordId = :wordId")
    fun getUser(wordId: Long): Word
}