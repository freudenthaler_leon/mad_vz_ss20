package com.example.libraryroomexample.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

// entities represent database tables
// specify table name (default would be "word")
@Entity(tableName = "word_table")
data class Word(
    @ColumnInfo(name = "word")
    val word: String
){
    // tables need primary keys
    // autoGenerate creates ids for us (like autoincrement in sql)
    @PrimaryKey(autoGenerate = true)
    var wordId: Long = 0L
}