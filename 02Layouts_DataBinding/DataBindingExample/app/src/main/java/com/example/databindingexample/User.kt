package com.example.databindingexample

//data classes are structured data containers whose main purpose is to hold data
//data classes have pre-defined standard and utility functions
//eg.: equals(), hashCode(), toString(), copy(), ...
data class User (var username: String = "", var email: String = ""){
    //properties outside data class constructor
    var isAdmin: Boolean = false

    //example of overriding provided toString() method
    override fun toString(): String {
        return "Username: " + username +
                " \nE-Mail: " + email
    }
}