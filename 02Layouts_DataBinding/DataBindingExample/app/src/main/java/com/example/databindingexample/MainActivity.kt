package com.example.databindingexample

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.example.databindingexample.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    //binding is declared as lateinit variable since it will be assigned in onCreate() method
    private lateinit var binding: ActivityMainBinding
    private val user: User = User("Jane Doe")
    private var hasChangedUsername: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)

        //generate binding object and save it to variable
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        //get btn from binding and assign it to variable
        var myBtn = binding.clickBtn

        //create new user data class
        //val user: User = User("Jane Doe")
        //assign data class to binding variable
        binding.userVarInXML = user

        //add an onClick listener to button
        myBtn.setOnClickListener{
            changeUsername(it)
        }

        //on button click event go to next activity
        //and pass username data
        binding.nextActivity.setOnClickListener {

            val dataToSend = user.username
            val intent = Intent(this, ViewBindingExampleActivity::class.java).apply {
                putExtra("username", dataToSend)    //put data in intent with username
            }

            //or use a bundle to pass data
            var bundleToSend = Bundle()
            bundleToSend.putString("username", user.username)
            bundleToSend.putBoolean("hasChangedUsername", hasChangedUsername)

            intent.putExtras(bundleToSend)

            startActivity(intent)   //start the second activity
        }

    }

    private fun changeUsername(view: View){
        if(hasChangedUsername == false) {
            hasChangedUsername = true
        }
        //toggle between Jane and John Doe
        user.username = when(user.username){
            "John Doe" -> "Jane Doe"
            "Jane Doe" -> "John Doe"
            else -> "Identified person"
        }


        binding.apply {
            invalidateAll()                         //invalidates all binding expressions and requests a new rebind to refresh UI
            //userVarInXML?.username = user.username  //reassign username
            //userVarInXML?.email = ""
        }
    }
}
