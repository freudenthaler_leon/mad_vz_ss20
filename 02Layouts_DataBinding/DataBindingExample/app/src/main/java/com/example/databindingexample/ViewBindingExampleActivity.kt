package com.example.databindingexample

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.databindingexample.databinding.ActivityViewBindingExampleBinding

class ViewBindingExampleActivity : AppCompatActivity() {

    private lateinit var binding: ActivityViewBindingExampleBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityViewBindingExampleBinding.inflate(layoutInflater)     //generate instance of binding class with view binding
        val view = binding.root     //get root view
        setContentView(view)        //set content with root view

        //button that navigates back to main activity
        binding.button.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java).apply {}
            startActivity(intent)
        }

        //get data from intent that started the activity
        val username = intent.getStringExtra("username")

        //get bundle from intent
        val bundle: Bundle? = intent.extras
        val hasUsernameChanged: Boolean? = bundle?.getBoolean("hasChangedUsername")     //safe call

        //set the textview text with data
        if(hasUsernameChanged != null && hasUsernameChanged == true) {
            binding.textView.text = "Username changed: " + username
        } else {
            binding.textView.text = username
        }
    }
}
