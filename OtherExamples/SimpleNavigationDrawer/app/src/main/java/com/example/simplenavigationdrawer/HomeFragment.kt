package com.example.simplenavigationdrawer

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.core.app.ShareCompat
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.simplenavigationdrawer.databinding.FragmentHomeBinding

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        /*
        // onCLickListener can also be added through navigation
        // this is not necessary, it can also be added the "normal" way - see in onViewCreated()
        binding.button.setOnClickListener {
            Navigation.createNavigateOnClickListener(R.id.action_homeFragment_to_anotherFragment)
        }
         */

        //tell android that we have a menu associated with this fragment
        setHasOptionsMenu(true)
        return binding.root
    }

    //this method is called when the menu is inflated
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.dropdown_menu, menu)   //tell which resource menu should be inflated
    }

    //this method is called if a menu item is selected
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item, requireView().findNavController())
                || return super.onOptionsItemSelected(item)


            /*
           //unsafe method from udacity
           return NavigationUI.onNavDestinationSelected(item, view!!.findNavController())
                || return super.onOptionsItemSelected(item)

             */

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.button.setOnClickListener {
            //navigate to another fragment
            //findNavController() gets the parent Nav Host Fragment from any view within the fragment
            findNavController().navigate(R.id.action_homeFragment_to_anotherFragment)

            //alternatively use navigation with safe-args
            //findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToAnotherFragment())
        }
    }
}
