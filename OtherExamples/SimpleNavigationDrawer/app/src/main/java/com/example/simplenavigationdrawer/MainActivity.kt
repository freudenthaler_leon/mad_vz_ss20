package com.example.simplenavigationdrawer

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.navigateUp
import com.example.simplenavigationdrawer.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        drawerLayout = binding.drawerLayout

        navController = findNavController(R.id.myNavHostFragment)

        //hookup NavigationUI to the action bar
        NavigationUI.setupActionBarWithNavController(this, navController, drawerLayout)
        //drawer layout
        NavigationUI.setupWithNavController(binding.navView, navController)
    }

    //handle what to do when up is pressed (for example the back arrow in the action bar)
    override fun onSupportNavigateUp(): Boolean {
        //return navController.navigateUp()     // if no navigation drawer is used
        return navController.navigateUp(drawerLayout)
    }
}
