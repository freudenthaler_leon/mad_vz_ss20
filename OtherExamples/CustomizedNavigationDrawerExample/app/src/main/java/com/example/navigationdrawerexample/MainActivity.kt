package com.example.navigationdrawerexample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.navigationdrawerexample.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        drawerLayout = binding.drawerLayout

        navController = this.findNavController(R.id.myNavHostFragment)

        //set the fragments that should implement the drawer menu
        val fragmentsWithMenuDrawer = setOf(R.id.aboutFragment,
                R.id.homeFragment)

        //set multiple top-level destinations so that menu drawer is shown in that fragments
        // JVM > 1.8 use:
        //      val appBarConfiguration = AppBarConfiguration(fragmentsWithMenuDrawer)
        appBarConfiguration = AppBarConfiguration.Builder(fragmentsWithMenuDrawer)
            .setDrawerLayout(drawerLayout)
            .build()

        // Set up ActionBar
        setSupportActionBar(binding.toolbar)
        setupActionBarWithNavController(navController, appBarConfiguration)

        // Set up navigation menu
        binding.navView.setupWithNavController(navController)
    }

    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(navController, appBarConfiguration)
    }

    override fun onBackPressed() {
        if(drawerLayout.isDrawerOpen(binding.root)){
            drawerLayout.closeDrawer(binding.root)
        }else{
            super.onBackPressed()
        }

        //GravityCompat is DEPRECATED
        /*
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
         */

    }
}
