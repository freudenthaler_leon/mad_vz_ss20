# Mobile App Development - SS 2020 #

This repository provides course contents, how-to files and coding examples of Mobile App Development course in Computer Science and Digital Communications (4. semester, SS20).

## Course Lectures ##

### 01 - Android and Kotlin ###

Kotlin and Android basics.

**Course Material**

Youtube Kotlin Tutorial: [Kotlin Basics Video Tutorial](https://www.youtube.com/playlist?list=PLNmsVeXQZj7rylgyThgUldHG8KE6Nbc1O)

Android Developers: [Build your first Android App](https://developer.android.com/training/basics/firstapp)

**Additional Material**

Kotlin Online Playground: [Kotlinlang Playground](https://play.kotlinlang.org/)

Learn Kotlin by Example: [Kotlinlang](https://play.kotlinlang.org/byExample)

Comparison between Kotlin and Java: [KotlinVSJava](https://www.kotlinvsjava.com/)

Kotlin and Android: [Raywenderlich Tutorial](https://www.raywenderlich.com/1144981-kotlin-for-android-an-introduction#toc-anchor-001)

Kotlin Basics Udacity: [Udacity Video Course](https://www.udacity.com/course/kotlin-bootcamp-for-programmers--ud9011)

Kotlin Cheat Sheet and Quick References: [Raywenderlich Kotlin Cheat Sheet](https://koenig-media.raywenderlich.com/uploads/2019/11/RW-Kotlin-Cheatsheet-1.1.pdf)

**Student Number Master Examples**

Coding examples of Learning Diary 01 can be found here: [Number Master Examples](https://bitbucket.org/freudenthaler_leon/mad_vz_ss20/src/master/01Android_Kotlin/Learning_Diary_01/)

### 02 - Layouts and Data Binding ###
Layout principles and data binding in Android app development with Kotlin.

**Course Material**

Udacity Course lesson 2: [](https://www.udacity.com/course/developing-android-apps-with-kotlin--ud9012)

**Additional Material**

[Layouts Overview](https://developer.android.com/guide/topics/ui/declaring-layout)

[Views Overview](https://github.com/codepath/android_guides/wiki/Constructing-View-Layouts)

[Constraint Layout](https://constraintlayout.com/basics/create_chains.html)

[ViewBinding](https://developer.android.com/topic/libraries/view-binding)

[ViewBinding Article](https://medium.com/google-developer-experts/exploring-view-binding-on-android-44e57ba11635)

[Differences ViewBinding and DataBinding](https://proandroiddev.com/new-in-android-viewbindings-the-difference-from-databinding-library-bef5945baf5e)

[Material Design Komponenten für Android Apps](https://www.material.io/components)

**Data Binding Example**

Data binding example can be found here: [Data Binding](https://bitbucket.org/freudenthaler_leon/mad_vz_ss20/src/master/02Layouts_DataBinding/)

### 03 - Fragments and Activities ###

### 04 - Lifecycle ###

### 05 - Room ###

