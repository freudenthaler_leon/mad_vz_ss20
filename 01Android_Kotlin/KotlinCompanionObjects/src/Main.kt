

//class to be used as companion object
//can be shared between classes eg.: use stateless code across several classes for internal purposes
open class Serializer {
    protected fun serialize(obj: Any): String{
        return "Fullname: " + obj.toString()
    }
}

class Student(firstname: String = "Jane", lastname: String = "Doe", semester: Int = 3) {
    var firstname = firstname
    var lastname = lastname

    companion object: Serializer()

    fun printFullName() {
        println(serialize(firstname + " " + lastname))
    }
}

class Lecturer(firstname: String = "John", lastname: String = "Doe") {
    var firstname = firstname
    var lastname = lastname
    //example of getter and setter in kotlin
    var courseCount: Int = 0
        get() = field
        set(value) {
            field = value
        }

    companion object: Serializer()

    fun printFullName() {
        println(serialize(firstname + " " + lastname))
    }
}

fun main(){
    var student: Student = Student("Bart")
    var lecturer: Lecturer = Lecturer()


    Student().printFullName()
    student.printFullName()
    lecturer.printFullName()
}