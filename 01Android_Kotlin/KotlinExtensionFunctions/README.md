# Extension Functions in Kotlin #

Extension functions in Kotlin are used to provide new functionality to existing classes, without having to inherit from the class or use design patterns such as Decorator.
Such functions are available for calling in the usual way as if they were methods of the original class.

In summary, extensions are useful tools to extend types that already exist in the system – either because they don't have the functionality we need or simply to make some specific area of code easier to manage.

## Usages of Extension Functions ##
Extensions on the other hand provide many use cases which can help developers of all levels improve their code by:

* Reducing the number of classes necessary to build a modular, object oriented application (helps to manage class explosions which are a natural result of applying separation of concerns in an information system such as a computer program)
* Extending the functionality of existing types, classes, and objects (source files), without requiring that you change their source code itself (for more information, look in to the Open-Closed Principle from SOLID Principles)
* Making application code less repetitive (therefore more concise) and more legible (assuming you don’t write ugly and illegible names for your extensions)

## Examples ##
```
// add a swap function to MutableList
fun MutableList<Int>.swap(index1: Int, index2: Int) {
    val tmp = this[index1] // 'this' corresponds to the list
    this[index1] = this[index2]
    this[index2] = tmp
}
```

```
// extension function that escapes string characters
fun String.escapeForXml() : String {
    return this
            .replace("&", "&amp;")
            .replace("<", "&lt;")
            .replace(">", "&gt;")
}
```
## Sources ##
[Kotlinlang Extension Functions](https://kotlinlang.org/docs/reference/extensions.html)

[How and when to use Extension Functions](https://medium.com/@rkay301/how-and-when-to-use-kotlin-extension-functions-bf92aec5ae1e)