


fun main(){
    var list = mutableListOf<Int>(1, 3, 5, 0)

    println(list)
    list.swap(0, list.lastIndex)
    println(list)

    var xml: String =
        """
            <note>
                <to>Tove</to>
                <from>Jani</from>
                <heading>Reminder</heading>
                <body>Don't forget me this weekend!</body>
            </note>
        """

    var escapedXML = xml.escapeForXml()
    print(escapedXML)
}

// add a swap function to MutableList
fun MutableList<Int>.swap(index1: Int, index2: Int) {
    val tmp = this[index1] // 'this' corresponds to the list
    this[index1] = this[index2]
    this[index2] = tmp
}

// extension function that escapes string characters
fun String.escapeForXml() : String {
    return this
        .replace("&", "&amp;")
        .replace("<", "&lt;")
        .replace(">", "&gt;")
}